/*
Samuel Kenney
Object Oriented/ Adv Programming: Program 1
9/6/2016
Implements functors for int, float, and string hash functions.
Driver for testing of Hash Template
*/

#include "HashTemplateChaining.h"
#include <iostream>
#include <string>

class intFunctor {
public:
	int operator()(int key){
		const double A = (sqrt(5)-1)/2;
		return static_cast<int>(((key * A) - floor(key * A)) * 1000000);
	}
};

class floatFunctor {
public:
	int operator()(float key){
		const double A = (sqrt(5)-1)/2;
		return static_cast<int>(((key * A) - floor(key * A)) * 1000000);
	}
};

class stringFunctor {
public:
	int operator()(std::string key){
		std::hash<std::string> stringFunct;
		size_t hashNum = stringFunct(key);
		return static_cast<int>(hashNum);
	}
};

int main (int argc, char**argv) {
	hashTemplate<int, int, intFunctor> intHash(.5, 10);
	intHash.insertHash(0, 100);
	intHash.insertHash(1234, 47);
	intHash.insertHash(9334, 63);
	intHash.insertHash(98434, 27);
	intHash.insertHash(343, 265);
	intHash.insertHash(340, 187);

	std::cout << "==========Testing 'int' type for keys.==========" << std::endl;
	intHash.searchKey(89);
	intHash.searchKey(265);
	intHash.searchKey(63);
	intHash.searchKey(987);
	std::cout << "================================================" <<std::endl;
	std::cout << std::endl;

	hashTemplate<int, float, floatFunctor> floatHash(.2, 10);
	floatHash.insertHash(5, 5.34f);
	floatHash.insertHash(4, 345.34f);
	floatHash.insertHash(234, 456.90f);
	floatHash.insertHash(98, 8.890f);

	std::cout << "==========Testing 'float' type for keys.===========" << std::endl;
	floatHash.searchKey(5.34f);
	floatHash.searchKey(345.34f);
	floatHash.searchKey(98.323f);
	std::cout << "===================================================" << std::endl;
	std::cout << std::endl;

	hashTemplate<std::string, std::string, stringFunctor> stringHash(.1, 10);
	stringHash.insertHash("Hello to you!", "Hello!");
	stringHash.insertHash("Rick!", "What is your name?");
	stringHash.insertHash("Cool!", "Mine is Mr. Meeseeks!");
	stringHash.insertHash("Nice!", "Look at me!");

	std::cout << "==========Testing 'string' type for keys.==========" << std::endl;
	stringHash.searchKey("Mine is Mr. Meeseeks!");
	stringHash.searchKey("Look at me!");
	stringHash.searchKey("Not in the hash table");
	std::cout << "===================================================" << std::endl;
	std::cout << std::endl;
	
	return 0;
}