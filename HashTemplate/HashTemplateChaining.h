/*
Samuel Kenney
Object Oriented/ Adv Programming: Program 1
9/6/2016
Implements Hash Template for Hash table using chainging. 
Vectors used rather than Linked Lists. 
*/

#pragma once
#include <vector>
#include <iostream>
//			dataType	keyType		operation
template<typename T, typename K, typename op>
class hashTemplate {
public:
	hashTemplate(double loadFactor, int initCap);
	void insertHash(T, K);
	void searchKey(K);
	void grow(void);
private:
	int numItems;
	int capacity;
	double loadFactor;
	std::vector<std::vector<std::pair<T, K>>> hash;
	op hashFunction;
};

//initializes hastTemplate class
template<typename T, typename K, typename op>
hashTemplate<T,K,op>::hashTemplate(double loadFactor, int initCap) {
	numItems = 0;
	capacity = initCap;
	this->loadFactor = loadFactor;
	hash.resize(initCap);
}

//calls functor to grab position to insert 
template<typename T, typename K, typename op>
void hashTemplate<T,K,op>::insertHash(T data, K key){
	numItems++;

	int position = (hashFunction(key))%capacity;
	if (position < 0) {
		position *= -1;
	}

	hash.at(position).push_back(std::make_pair(data, key));

	//cast to doubles to not lose decimel values
	double doubleItems, doubleCap;
	doubleItems = numItems;
	doubleCap = capacity;
	//checks if loadFactor is reached
	if ((doubleItems/doubleCap) >= loadFactor){
		grow();
	}
}

//find the key in the hash table
template<typename T, typename K, typename op>
void hashTemplate<T,K,op>::searchKey(K key){
	int position = (hashFunction(key))%capacity;
	if (position < 0) {
		position *= -1;
	}
	//uses hash function to find key
	bool found = false;
	for (int i = 0; i < hash.at(position).size(); i++){
		if (hash.at(position).at(i).second == key) {
			found = true;
			std::cout << "Using key '" << key << "', the data is '" << hash.at(position).at(i).first << "'" << std::endl;
		}
	}

	if (found == false) {
		std::cout << "Sorry, that key is not in this hash table." << std::endl;
	}
}

//if the load factor is reached, the vector is doubled
template<typename T, typename K, typename op>
void hashTemplate<T,K,op>::grow(void){
	numItems = 0;

	std::vector<std::vector<std::pair<T, K>>> hashTemp;
	hashTemp.resize(capacity);

	//copy over to temp
	for (int i = 0; i < hash.size(); i++ ) {
		for (int j = 0; j < hash.at(i).size(); j++) {
			hashTemp.at(i).push_back(std::make_pair(hash.at(i).at(j).first,hash.at(i).at(j).second));
		}
	}

	//clean out old hash table
	hash.clear();
	capacity = capacity*2;
	hash.resize(capacity);

	//insert newly hashed values into hash table
	for (int i = 0; i < hashTemp.size(); i++ ) {
		for (int j = 0; j < hashTemp.at(i).size(); j++) {
			insertHash(hashTemp.at(i).at(j).first, hashTemp.at(i).at(j).second);
		}
	}
}